import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { CourseComponent } from '../course/course.component';
import { PageNotFoundComponent } from '../core/components/page-not-found/page-not-found.component';
import { CourseItemDataComponent } from '../course/components/course-item-data/course-item-data.component';
import { LoginPageComponent } from '../core/components/login-page/login-page.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: '',
    redirectTo: 'courses',
    pathMatch: 'full',
    canActivate: [AuthService]
  },
  {
    path: 'courses',
    component: CourseComponent,
    canActivate: [AuthService]
  },
  {
    path: 'courses/edit',
    component: CourseItemDataComponent,
    canActivate: [AuthService]
  },
  {
    path: 'courses/new',
    component: CourseItemDataComponent,
    canActivate: [AuthService]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [AuthService]
})
export class AppRoutingModule {
  constructor(private authService: AuthService) {
  }
}
