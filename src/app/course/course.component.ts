import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'vp-courses',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  public searchString: string;
  public url: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.searchString = '';
    this.url = this.router.url;
  }

  onSearch(value) {
    this.searchString = value;
  }

}
