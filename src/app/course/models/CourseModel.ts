export interface ICourseItem {
  id: string;
  title: string;
  creationDate: number;
  duration: number;
  description: string;
  authors?: string[];
  isTopRated?: boolean;
}

export interface IHighlightCondition {
  isFresh: boolean;
  isUpcoming: boolean;
}

export enum sortByTypes {
  createdDateASC = 'createdDateASC',
  createdDateDSC = 'createdDateDSC',
  durationASC = 'durationASC',
  durationDSC = 'durationDSC',
}
