import { TestBed } from '@angular/core/testing';
import { CourseService } from './course.service';
import { list as mockCoursesList } from '../mocks/courses.json';
import { ICourseItem } from '../models/CourseModel';
import { CourseModule } from '../course.module';

describe('CourseService', () => {
  let service: CourseService;
  let data: ICourseItem[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CourseModule]
    });
    service = TestBed.inject(CourseService);
    data = service.getList();
    spyOn(window, 'confirm').and.returnValue(true);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should remove course from list', () => {
    expect(data.length === mockCoursesList.length).toBe(true);
    const firstCourse = data[0];
    data = service.removeItem(firstCourse);
    expect(data.length < mockCoursesList.length).toBe(true);
    expect(data.find((c) => c.id === firstCourse.id)).toBe(undefined);
  });

  it('should add course to list', () => {
    const newCourse = {
      id: 'course-test',
      title: 'new course',
      creationDate: new Date().getTime(),
      duration: 0,
      description: '',
    };
    expect(data.find((c) => c.id === newCourse.id)).toBe(undefined);
    service.createCourse(newCourse);
    data = service.getList();
    expect(data.find((c) => c.id === newCourse.id)).toBe(newCourse);
  });

  it('should returns course by id', () => {
    const firstCourse = data[0];
    expect(service.getItemById(firstCourse.id)).toEqual(firstCourse);
  });

  it('should updates course', () => {
    const firstCourse = data[0];
    const newCourseData = {
      id: firstCourse.id,
      title: 'new course',
      creationDate: new Date().getTime(),
      duration: 0,
      description: '',
    };
    service.updateItem(newCourseData);
    data = service.getList();
    expect(data[0]).toEqual(newCourseData);
  });
});
