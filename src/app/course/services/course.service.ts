import { Injectable } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { ICourseItem } from '../models/CourseModel';
import { list as mockCoursesList } from '../mocks/courses.json';

@Injectable({
  providedIn: CoreModule,
})
export class CourseService {
  private data: ICourseItem[] = mockCoursesList;

  constructor() { }

  public getList(): ICourseItem[]{
    return this.data || [];
  }

  public createCourse(course: ICourseItem): void {
    this.data.push(course);
    console.log(`course with id=${course.id} has been created`);
  }

  public getItemById(id: string): ICourseItem | undefined {
    console.log(`course with id=${id} has been found`);
    return this.data.find((course: ICourseItem): boolean => course.id === id);
  }

  public updateItem(course: ICourseItem): void {
    const courseIndex = this.data.findIndex((c: ICourseItem) => c.id === course.id);
    if (courseIndex > -1) {
      this.data[courseIndex] = Object.assign(course);
      console.log(`course with id=${course.id} has been updated`);
    }
    console.log(`course with id=${course.id} not found`);
  }

  public removeItem(course: ICourseItem): ICourseItem[] {
    const check = confirm('Do you really want to delete this course?');
    if (check) {
      this.data = this.data.filter(c => c.id !== course.id);
      console.log(`course with id=${course.id} has been deleted`);
    }
    return this.data;
  }

  public createNewItem(): ICourseItem {
    return {
      id: `course-${Math.random()}`,
      title: '',
      creationDate: new Date().getTime(),
      duration: 0,
      description: '',
      authors: ['John Doe'],
      isTopRated: false,
    };
  }
}
