import { Pipe, PipeTransform } from '@angular/core';
import { ICourseItem } from '../../models/CourseModel';

@Pipe({
  name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {
  transform(listOfCourses: ICourseItem[], filterBy: string): ICourseItem[] {
    return listOfCourses.filter((course) => {
      const { title, description } = course;
      return title.includes(filterBy) || description.includes(filterBy);
    }).slice();
  }
}
