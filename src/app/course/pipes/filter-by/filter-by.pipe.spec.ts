import { FilterByPipe } from './filter-by.pipe';
import * as courses from '../../mocks/courses.json';

describe('FilterByPipe', () => {
  let pipe;
  beforeEach(() => {
    pipe = new FilterByPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should filtered array', () => {
    expect(pipe.transform(courses.list, '88').length).toEqual(1);
  });
});
