import { FormatTimePipe } from './format-time.pipe';

describe('FormatTimePipe', () => {
  it('create an instance', () => {
    const pipe = new FormatTimePipe();
    expect(pipe).toBeTruthy();
  });

  it('should format minutes to h:mm', () => {
    const pipe = new FormatTimePipe();
    expect(pipe.transform(100)).toBe('1h 40m');
  });
});
