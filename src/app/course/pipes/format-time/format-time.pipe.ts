import { Pipe, PipeTransform } from '@angular/core';
import { minsToHoursMins } from '../../../utils/time';

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {
  transform(value: number): string {
    const mToHM = minsToHoursMins(value);
    const hours = mToHM.hours > 0 ? `${ mToHM.hours }h ` : '';
    const minutes = `${ mToHM.mins }m`;
    return `${ hours }${ minutes }`;
  }

}
