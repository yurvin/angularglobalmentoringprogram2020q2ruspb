import { SortByPipe } from './sort-by.pipe';
import * as courses from '../../mocks/courses.json';
import { sortByTypes } from '../../models/CourseModel';

describe('SortByPipe', () => {
  let pipe;

  beforeEach(() => {
    pipe = new SortByPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should sort by durationASC', () => {
    const sortedArray = pipe.transform(courses.list, sortByTypes.durationASC);
    const first = sortedArray[0];
    const last = sortedArray[sortedArray.length - 1];
    expect(last.duration >= first.duration).toBe(true);
  });

  it('should sort by durationDSC', () => {
    const sortedArray = pipe.transform(courses.list, sortByTypes.durationDSC);
    const first = sortedArray[0];
    const last = sortedArray[sortedArray.length - 1];
    expect(first.duration >= last.duration).toBe(true);
  });

  it('should sort by createdDateASC', () => {
    const sortedArray = pipe.transform(courses.list, sortByTypes.createdDateASC);
    const first = sortedArray[0];
    const last = sortedArray[sortedArray.length - 1];
    expect(last.creationDate >= first.creationDate).toBe(true);
  });

  it('should sort by createdDateDSC', () => {
    const sortedArray = pipe.transform(courses.list, sortByTypes.createdDateDSC);
    const first = sortedArray[0];
    const last = sortedArray[sortedArray.length - 1];
    expect(first.creationDate >= last.creationDate).toBe(true);
  });
});
