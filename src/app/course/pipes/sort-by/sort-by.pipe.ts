import { Pipe, PipeTransform } from '@angular/core';
import { ICourseItem } from '../../models/CourseModel';
import { sortByTypes } from '../../models/CourseModel';

@Pipe({
  name: 'sortBy',
})

/* ToDo: this way is not recommended.
          Sorting should be implemented in the component.
          https://angular.io/guide/pipes#built-in-pipes
*/
export class SortByPipe implements PipeTransform {
  transform(listOfCourses: ICourseItem[], sortBy: string): ICourseItem[] {
    let sortedList: ICourseItem[];
    switch (sortBy) {
      case sortByTypes.createdDateASC:
        sortedList = listOfCourses.sort((item1, item2) => item1.creationDate - item2.creationDate);
        break;
      case sortByTypes.createdDateDSC:
        sortedList = listOfCourses.sort((item1, item2) => item2.creationDate - item1.creationDate);
        break;
      case sortByTypes.durationASC:
        sortedList = listOfCourses.sort((item1, item2) => item1.duration - item2.duration);
        break;
      case sortByTypes.durationDSC:
        sortedList = listOfCourses.sort((item1, item2) => item2.duration - item1.duration);
        break;
      default:
        sortedList = listOfCourses.slice();
    }
    return sortedList;
  }
}
