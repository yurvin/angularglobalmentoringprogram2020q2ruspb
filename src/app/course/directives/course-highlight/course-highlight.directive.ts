import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { IHighlightCondition } from '../../models/CourseModel';

@Directive({
  selector: '[vpCourseHighlight]'
})

export class CourseHighlightDirective implements OnInit {

  @Input('vpCourseHighlight') public highlight: IHighlightCondition;

  constructor(private element: ElementRef) {}

  ngOnInit(): void {
    if (this.highlight.isFresh) {
      this.element.nativeElement.style.border = '1px solid #00FF00';
      this.element.nativeElement.style.boxShadow = '0 0 4px #00FF00';
    } else if (this.highlight.isUpcoming) {
      this.element.nativeElement.style.border = '1px solid #00BFFF';
      this.element.nativeElement.style.boxShadow = '0 0 4px #00BFFF';
    }
  }
}
