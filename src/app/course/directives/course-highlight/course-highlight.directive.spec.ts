import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { CourseHighlightDirective } from './course-highlight.directive';

@Component({
  template: `<div class="course-item" [vpCourseHighlight]="{ isUpcoming: isUpcoming, isFresh: isFresh }">Test component</div>`
})
class TestComponent {
  isUpcoming: any;
  isFresh: any;
}

describe('CourseHighlightDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let component: TestComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseHighlightDirective, TestComponent ]
    }).compileComponents();
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
  }));

  it('should create an instance', async(() => {
    expect(component).toBeTruthy();
    const compiled = fixture.debugElement.nativeElement;
    const directive = new CourseHighlightDirective(compiled);
    expect(directive).toBeTruthy();
  }));
});
