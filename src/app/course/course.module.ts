import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing/app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CourseComponent } from './course.component';
import { BreadCrumbsComponent } from './components/bread-crumbs/bread-crumbs.component';
import { CourseItemComponent } from './components/course-item/course-item.component';
import { ListOfCoursesComponent } from './components/list-of-courses/list-of-courses.component';
import { AddCourseComponent } from './components/add-course/add-course.component';
import { SearchComponent } from './components/search/search.component';
import { LoadMoreComponent } from './components/load-more/load-more.component';

import { CourseHighlightDirective } from './directives/course-highlight/course-highlight.directive';
import { FormatTimePipe } from './pipes/format-time/format-time.pipe';
import { SortingComponent } from './components/sorting/sorting.component';
import { SortByPipe } from './pipes/sort-by/sort-by.pipe';
import { FilterByPipe } from './pipes/filter-by/filter-by.pipe';

import { CourseService } from './services/course.service';
import { CourseItemDataComponent } from './components/course-item-data/course-item-data.component';

@NgModule({
  declarations: [
    CourseComponent,
    BreadCrumbsComponent,
    CourseItemComponent,
    ListOfCoursesComponent,
    AddCourseComponent,
    SearchComponent,
    LoadMoreComponent,
    CourseHighlightDirective,
    FormatTimePipe,
    SortingComponent,
    SortByPipe,
    FilterByPipe,
    CourseItemDataComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
  ],
  exports: [
    CourseComponent,
  ],
  providers: [
    CourseService,
  ]
})
export class CourseModule { }
