import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'vp-load-more',
  templateUrl: './load-more.component.html',
  styleUrls: ['./load-more.component.scss']
})
export class LoadMoreComponent implements OnInit {
  @Input() public countOfCourses: number;

  constructor() { }

  ngOnInit(): void {
  }

  loadMore(): void {
    console.log('loadMore');
  }

}
