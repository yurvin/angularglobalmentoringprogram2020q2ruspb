import { Component, OnInit, Input } from '@angular/core';
import { upperFirst } from '../../../utils/string';

@Component({
  selector: 'vp-bread-crumbs',
  templateUrl: './bread-crumbs.component.html',
  styleUrls: ['./bread-crumbs.component.scss']
})
export class BreadCrumbsComponent implements OnInit {
  @Input()url: string;
  public pages: { page: string, id: string }[];
  constructor() { }

  ngOnInit(): void {
    this.pages = this.url
      .split('/')
      .filter(page => page.length)
      .map((page) => {
        const id = page.includes('id') ? page.substring(page.indexOf('id=') + 3, page.length) : '';
        const clearPage = page.includes('?') ? page.substring(0, page.indexOf('?')) : page;
        return { page: clearPage, id };
      });
  }

}
