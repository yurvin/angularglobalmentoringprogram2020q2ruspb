import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'vp-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public faSearch = faSearch;
  public searchString: string;
  @Output() public onSearch = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.searchString = '';
  }

  search() {
    this.onSearch.emit(this.searchString);
  }
}
