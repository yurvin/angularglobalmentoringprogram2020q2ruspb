import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ICourseItem } from '../../models/CourseModel';
import {
  faClock,
  faCalendarAlt,
  faPen,
  faTrash,
  faStar,
} from '@fortawesome/free-solid-svg-icons';
import { timesToMs } from '../../../utils/time';

@Component({
  selector: 'vp-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.scss'],
})
export class CourseItemComponent implements OnInit {
  public faClock = faClock;
  public faCalendarAlt = faCalendarAlt;
  public faPen = faPen;
  public faTrash = faTrash;
  public faStar = faStar;

  @Input()
  public courseItem: ICourseItem = {
    id: '',
    title: '',
    creationDate: 0,
    duration: 0,
    description: '',
    authors: [],
    isTopRated: false,
  };
  @Input() chooseTypeSorting: string;
  @Output() removeCourse = new EventEmitter<ICourseItem>();
  @Output() isFresh = false;
  @Output() isUpcoming = false;

  constructor(private router: Router) {}

  ngOnInit(): void {
    const now = new Date().getTime();
    const { creationDate } = this.courseItem;
    this.isFresh = creationDate < now && creationDate >= now - timesToMs(14, 'day');
    this.isUpcoming = creationDate > now;
  }

  editCourse(): void {
    this.router.navigate([`courses/edit`], {queryParams: { id: this.courseItem.id }});
  }

  deleteCourse(): void {
    this.removeCourse.emit(this.courseItem);
  }
}
