import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { sortByTypes } from '../../models/CourseModel';

@Component({
  selector: 'vp-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.scss']
})
export class SortingComponent implements OnInit {

  @Output() public onChanged = new EventEmitter<string | null>();
  public sortBy: string[] = Object.keys(sortByTypes);
  public isChoosedSortingType = false;

  constructor() { }

  ngOnInit(): void { }

  onOptionsSelected(value: string) {
    this.isChoosedSortingType = !!value;
    this.onChanged.emit(value);
  }
}
