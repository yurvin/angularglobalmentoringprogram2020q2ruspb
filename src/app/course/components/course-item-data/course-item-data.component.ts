import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICourseItem } from '../../models/CourseModel';
import { CourseService } from '../../services/course.service';

@Component({
  selector: 'vp-course-item-data',
  templateUrl: './course-item-data.component.html',
  styleUrls: ['./course-item-data.component.scss']
})
export class CourseItemDataComponent implements OnInit {

  isReadyToSave = false;
  public courseId: string;
  public course: ICourseItem;
  private creationDateTmp: number;
  public url: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private courseService: CourseService
  ) { }

  ngOnInit(): void {
    this.courseId = this.route.snapshot.queryParams.id;
    if (this.courseId) {
      this.course = this.courseService.getItemById(this.courseId);
      this.isReadyToSave = true;
    } else {
      this.course = this.courseService.createNewItem();
    }

    this.creationDateTmp = this.course.creationDate;
    this.url = this.router.url;
  }

  save(): void {
    this.router.navigate(['courses']).then(() => {
      this.course.creationDate = this.creationDateTmp;
      this.courseService.updateItem(this.course);
    });
  }

  cancel(): void {
    this.router.navigate(['courses']);
  }

  change() {
    let valuesCourse = Object.values(this.course);
    valuesCourse = valuesCourse.slice(0, 5);
    this.isReadyToSave = valuesCourse.find(value => !!value === false) !== -1;
  }

  formatDate(creationDate) {
    this.creationDateTmp = new Date(creationDate).getTime();
  }
}
