import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CourseItemDataComponent } from './course-item-data.component';

describe('CourseItemDataComponent', () => {
  let component: CourseItemDataComponent;
  let fixture: ComponentFixture<CourseItemDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
