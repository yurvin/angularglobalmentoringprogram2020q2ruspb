import { Component, Input, OnInit, Output } from '@angular/core';
import { ICourseItem } from '../../models/CourseModel';
import { CourseService } from '../../services/course.service';

@Component({
  selector: 'vp-list-of-courses',
  templateUrl: './list-of-courses.component.html',
  styleUrls: ['./list-of-courses.component.scss'],
  providers: [CourseService],
})
export class ListOfCoursesComponent implements OnInit {

  public courses: ICourseItem[];
  public chooseTypeSorting: string;
  @Input() public searchString: string;
  @Output() public countOfCourses: number;

  constructor(private courseService: CourseService) { }

  ngOnInit(): void {
    this.courses = this.courseService.getList();
    this.countOfCourses = this.courses.length;
  }

  removeCourse(course: ICourseItem) {
    this.courses = this.courseService.removeItem(course);
  }

  onChanged(value) {
    this.chooseTypeSorting = value;
  }
}
