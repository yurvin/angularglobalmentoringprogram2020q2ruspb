import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfCoursesComponent } from './list-of-courses.component';
import { SortByPipe } from '../../pipes/sort-by/sort-by.pipe';
import { FilterByPipe } from '../../pipes/filter-by/filter-by.pipe';

describe('ListOfCoursesComponent', () => {
  let component: ListOfCoursesComponent;
  let fixture: ComponentFixture<ListOfCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfCoursesComponent, SortByPipe, FilterByPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
