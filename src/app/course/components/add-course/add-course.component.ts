import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'vp-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent implements OnInit {
  faPlus = faPlus;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  addCourse(): void {
    this.router.navigate(['courses/new']);
  }
}
