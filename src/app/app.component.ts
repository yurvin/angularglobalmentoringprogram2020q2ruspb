import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'vp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AuthService],
})
export class AppComponent implements OnInit {
  title = 'video-portal';
  public isShowLogin = false;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.onLogin.subscribe(() => {
      this.isShowLogin = this.authService.isShowLogin;
    });
  }
}
