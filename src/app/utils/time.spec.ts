import * as timeUtils from './time';
import { unitsOfTime } from './mocks/time.mock.json';

describe('utils - time', () => {
  it('should convert units of time milliseconds', () => {
    expect(timeUtils.timesToMs(1, 'second')).toBe(unitsOfTime.second);
    expect(timeUtils.timesToMs(1, 'minute')).toBe(unitsOfTime.minute);
    expect(timeUtils.timesToMs(1, 'hour')).toBe(unitsOfTime.hour);
    expect(timeUtils.timesToMs(1, 'day')).toBe(unitsOfTime.day);
    expect(timeUtils.timesToMs(1, 'month')).toBe(unitsOfTime.month);
    expect(timeUtils.timesToMs(1, 'year')).toBe(unitsOfTime.year);
  });
});


