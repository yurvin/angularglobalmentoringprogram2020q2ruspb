export const timesToMs = (count: number, type: string): number | null => {
  const enum TimeTypes {
    second = 'second',
    minute = 'minute',
    hour = 'hour',
    day = 'day',
    month = 'month',
    year = 'year',
  }
  let res: number | null;
  switch (type) {
    case TimeTypes.second:
      res = count * 1000;
      break;
    case TimeTypes.minute:
      res = timesToMs(count * 60, TimeTypes.second);
      break;
    case TimeTypes.hour:
      res = timesToMs(count * 60, TimeTypes.minute);
      break;
    case TimeTypes.day:
      res = timesToMs(count * 24, TimeTypes.hour);
      break;
    case TimeTypes.month:
      res = count * 2592000000;
      break;
    case TimeTypes.year:
      res = count * 31556952000;
      break;
    default:
      console.error(`invalid time type!`);
      res = null;
  }

  return res;
};

export const minsToHoursMins = (mins: number): { hours: number, mins: number } => {
  const hours = (mins / 60);
  const rhours = Math.floor(hours);
  const minutes = (hours - rhours) * 60;
  return {
    hours: Math.floor(hours),
    mins:  Math.round(minutes),
  };
};

