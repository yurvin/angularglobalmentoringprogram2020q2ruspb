export interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  token?: string;
}

export interface ILoginParams {
  eMail: string;
  password: string;
}
