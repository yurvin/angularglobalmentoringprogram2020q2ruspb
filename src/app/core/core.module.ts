import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LogoComponent } from './components/header/logo/logo.component';
import { LogInOffComponent } from './components/header/log-in-off/log-in-off.component';
import { UserLoginComponent } from './components/header/user-login/user-login.component';
import { CopyrightComponent } from './components/footer/copyright/copyright.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LogoComponent,
    LogInOffComponent,
    UserLoginComponent,
    CopyrightComponent,
    LoginPageComponent,
    PageNotFoundComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    LoginPageComponent,
  ],
})
export class CoreModule { }
