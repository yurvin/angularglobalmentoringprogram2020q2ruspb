import { Component, OnInit,  } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { ILoginParams } from '../../models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'vp-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  public loginParams: ILoginParams = {
    eMail: '',
    password: '',
  };

  public isActive = !!(this.loginParams.eMail.length && this.loginParams.password.length);

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  onClick() {
    if (this.isActive) {
      this.authService.login(this.loginParams);
      this.authService.showHideLoginPage(false);
      this.router.navigate(['courses']);
    }
  }

  change() {
    this.isActive = !!(this.loginParams.eMail.length && this.loginParams.password.length);
  }

}
