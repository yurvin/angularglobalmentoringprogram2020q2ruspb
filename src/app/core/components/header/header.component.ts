import { Component, OnInit } from '@angular/core';
import { IUser } from '../../models/User';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'vp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public user: IUser | null;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {}

  loginAuth() {
    if (this.user) {
      this.authService.logout();
    } else {
      this.authService.login();
    }
  }

}
