import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faSignInAlt, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { IUser } from '../../../models/User';
import { AuthService } from '../../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'vp-log-in-off',
  templateUrl: './log-in-off.component.html',
  styleUrls: ['./log-in-off.component.scss'],
})
export class LogInOffComponent implements OnInit {
  public faSignInAlt = faSignInAlt;
  public faSignOutAlt = faSignOutAlt;
  @Input() public user: IUser | null;

  @Output() loginAuth = new EventEmitter();
  public isAuth: boolean;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isAuth = this.authService.isAuthenticated();
    this.authService.onLogin.subscribe(() => {
      this.isAuth = this.authService.isAuthenticated();
    });
  }

  login(): void {
    if (this.isAuth) {
      this.router.navigate(['403']).then(() => {
        this.authService.logout();
        this.authService.showHideLoginPage(false);
      });
    } else {
      this.router.navigate(['login']).then(() => this.authService.showHideLoginPage(true));
    }
  }
}
