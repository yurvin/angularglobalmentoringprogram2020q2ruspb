import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogInOffComponent } from './log-in-off.component';

describe('LogInOffComponent', () => {
  let component: LogInOffComponent;
  let fixture: ComponentFixture<LogInOffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogInOffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogInOffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
