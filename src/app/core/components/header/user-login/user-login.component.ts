import { Component, Input, OnInit } from '@angular/core';
import { IUser } from '../../../models/User';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'vp-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent implements OnInit {
  faUserAlt = faUserAlt;

  @Input()
  public user: IUser | null;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.user = this.authService.getUserInfo();
    this.authService.onLogin.subscribe(() => {
      this.user = this.authService.getUserInfo();
    });
  }

}
