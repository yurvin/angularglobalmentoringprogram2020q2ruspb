import { Injectable } from '@angular/core';
import { IUser, ILoginParams } from '../core/models/User';
import { Subject, Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

const testUser: IUser = {
  id: `user-0`,
  firstName: 'John',
  lastName: 'Doe',
};

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  private userKey = 'vp-user';
  private isLogin: Subject<boolean> = new Subject<boolean>();
  public onLogin = this.isLogin.asObservable();
  public user: IUser | null;
  public isShowLogin = false;

  constructor(private router: Router) {}

  login(loginParams?: ILoginParams) {
    this.user = { ...testUser };
    this.user.token = this.generateToken();
    localStorage.setItem(this.userKey, JSON.stringify(this.user));
  }

  logout() {
    localStorage.removeItem(this.userKey);
    this.user = null;
  }

  isAuthenticated(): boolean {
    return !!JSON.parse(localStorage.getItem(this.userKey));
  }

  getUserInfo(): IUser | null {
    if (this.isAuthenticated()) {
      const user = JSON.parse(localStorage.getItem(this.userKey));
      return {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
      };
    }
    return null;
  }

  showHideLoginPage(showLoginPage: boolean): void {
    this.isShowLogin = showLoginPage;
    this.isLogin.next();
  }

  private generateToken(): string {
    const rand = () => Math.random().toString(36).substr(2);
    return rand() + rand();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const isAuth = this.isAuthenticated();

    if (!isAuth) {
      this.router.navigate(['403']);
    }
    return isAuth;
  }
}
